# dashboard
Web dashboard, application can be accessed via the web https://adi-dashboard.netlify.app/

## Tech Stacks

In this project we are using these:
- react [Documentation](https://reactjs.org/docs/getting-started.html)
- react-router-dom [Documentation](https://reactjs.org/docs/getting-started.html)
- node-sass [Documentation](https://sass-lang.com/documentation)
- react-chartjs-2 [Documentation](https://www.npmjs.com/package/react-chartjs-2)
- react-bootstrap-daterangepicker [Documentation](https://www.npmjs.com/package/react-bootstrap-daterangepicker)

### Running on Local Machine

You’ll need to have Node >= 10 on your local development machine (but it’s not required on the server). You can use nvm (macOS/Linux) or nvm-windows to switch Node versions between different projects. After all requirements meets then you need to run this command.

```bash
npm install
npm start
```

## Deployment Status

This App has been deployed on Netlify

[![Netlify Status](https://api.netlify.com/api/v1/badges/bf4ce0db-606d-4afa-971e-4bce94861d69/deploy-status)](https://app.netlify.com/sites/adi-dashboard/deploys)
