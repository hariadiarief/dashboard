import React, { useState } from 'react'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import moment from 'moment'
import { DateRange, ExpandLess, ExpandMore } from '@material-ui/icons'

export default function DateRangePickerCustom(props) {
	const ranges = {
		Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		'Last 7 Days': [moment().subtract(6, 'days'), moment().subtract(1, 'days')],
		'Last 30 Days': [moment().subtract(29, 'days'), moment().subtract(1, 'days')],
		'This Month': [moment().startOf('month'), moment().subtract(1, 'days')],
	}

	const minDate = moment().subtract(6, 'month')
	const maxDate = moment().subtract(1, 'days')

	const [isCalendarShow, setIsCalendarShow] = useState(false)

	const handleCallback = (start, end) => {
		setIsCalendarShow(!isCalendarShow)
		props.onChange(start, end)
	}

	return (
		<DateRangePicker
			initialSettings={{
				startDate: props.startDate,
				endDate: props.endDate,
				minDate,
				maxDate,
				ranges,
				opens: 'left',
				alwaysShowCalendars: true,
			}}
			onCallback={handleCallback}>
			<div>
				<button className='date-custome__button' onClick={() => setIsCalendarShow(!isCalendarShow)}>
					<DateRange />
					<span>
						Periode {moment(props.startDate).format('DD MMMM YYYY')}- {moment(props.endDate).format('DD MMMM YYYY')}
					</span>
					{isCalendarShow ? <ExpandLess /> : <ExpandMore />}
				</button>
			</div>
		</DateRangePicker>
	)
}
