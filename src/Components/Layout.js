import React from 'react'
import { NavLink } from 'react-router-dom'

export default function Layout({ children }) {
	return (
		<div className='layout'>
			<header className='layout__header'>
				<img src={require('Assets/logo/advotics.png').default} alt='' />
				<div className='layout__header__user'>
					<div>
						<span>Username</span>
						<span>Company Name</span>
					</div>
					<img src={require('Assets/dummy/face.jpg').default} alt='' />
					<img src={require('Assets/icon/logout.png').default} alt='' />
				</div>
				<div></div>
			</header>
			<main className='layout__main'>
				<aside className='layout__aside'>
					<div>
						<img src={require('Assets/icon/menu.svg').default} alt='' />
					</div>
					<NavLink to='/' exact className='layout__aside__navigation' activeClassName='layout__aside__navigation--active'>
						<img src={require('Assets/icon/dashboard-icon.png').default} alt='' />
					</NavLink>
					<NavLink to='/sales' exact className='layout__aside__navigation' activeClassName='layout__aside__navigation--active'>
						<img src={require('Assets/icon/sales-turnover.svg').default} alt='' />
					</NavLink>
				</aside>
				<div className='layout__content'>{children}</div>
			</main>
		</div>
	)
}
