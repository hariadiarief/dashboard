import React from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'

export default function Chart({ startDate, endDate }) {
	let suffix1 = moment(startDate).format('DM') + moment(endDate).format('DM') - 15
	let suffix2 = moment(startDate).format('DM') + moment(endDate).format('DM') - 20
	let suffix3 = moment(startDate).format('DM') + moment(endDate).format('DM') - 30
	let suffix4 = moment(startDate).format('DM') + moment(endDate).format('DM') - 10
	let suffix5 = moment(startDate).format('DM') + moment(endDate).format('DM') - 25
	let suffix6 = moment(startDate).format('DM') + moment(endDate).format('DM') - 11
	let suffix7 = moment(startDate).format('DM') + moment(endDate).format('DM') - 25

	const data = {
		datasets: [
			{
				data: [
					15000 + suffix3 + suffix1 + suffix6 + suffix4,
					13000 + suffix4 + suffix5 + suffix3 + suffix1,
					10000 + suffix1 + suffix2 + suffix1 + suffix5,
					12000 + suffix5 + suffix6 + suffix5 + suffix6,
					15000 + suffix7 + suffix4 + suffix2 + suffix3,
					16000 + suffix2 + suffix7 + suffix4 + suffix7,
					17000 + suffix6 + suffix3 + suffix5 + suffix2,
				],
				fill: false,
				borderColor: '#FFE854',
				borderWidth: 3,
				type: 'line',
				legend: false,
			},
			{
				label: 'Nett',
				backgroundColor: '#37B04C',
				barThickness: 20,
				data: [14150 + suffix3, 12690 + suffix4, 9370 + suffix1, 10975 + suffix5, 14485 + suffix7, 15290 + suffix2, 16695 + suffix6],
			},
			{
				label: 'Gross',
				backgroundColor: '#289E45',
				barThickness: 20,
				data: [500 + suffix1, 100 + suffix5, 200 + suffix2, 400 + suffix6, 300 + suffix4, 500 + suffix7, 100 + suffix3],
			},
			{
				label: 'Average Purchase Value',
				backgroundColor: '#6BE681',
				barThickness: 20,
				data: [300 + suffix6, 200 + suffix3, 400 + suffix1, 600 + suffix5, 200 + suffix2, 200 + suffix7, 200 + suffix4, 150 + suffix5],
			},
			{
				label: 'Unit per Transaction',
				backgroundColor: '#707070',
				barThickness: 20,
				data: [50 + suffix4, 10 + suffix1, 30 + suffix5, 25 + suffix6, 15 + suffix3, 10 + suffix7, 55 + suffix2],
			},
		],
		labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
	}

	const options = {
		responsive: true,
		legend: {
			position: 'bottom',
			labels: {
				filter: function (item) {
					return item.text
				},
			},
		},
		tooltips: {
			mode: 'index',
			intersect: true,
		},
		scales: {
			xAxes: [
				{
					stacked: true,
				},
			],
			yAxes: [
				{
					stacked: true,
				},
			],
		},
	}

	return <Bar data={data} options={options} width={500} height={500} />
}
