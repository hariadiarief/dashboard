import React from 'react'

export default function Products({ data }) {
	return (
		<div className='home__product-list'>
			{data.map((item) => (
				<div className='home__product-list__item'>
					<img src={item.thumbnail} alt='' />
					<div>
						<span>{item.sku}</span>
						<div>
							<span>Rp. {new Intl.NumberFormat('id').format(item.price)}</span>
							<span>sold: {new Intl.NumberFormat('id').format(item.sold)}</span>
						</div>
					</div>
				</div>
			))}
		</div>
	)
}
