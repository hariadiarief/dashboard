import React, { useState } from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { MoreVert, ExpandLess, ExpandMore } from '@material-ui/icons'

import { Layout, DateRangePickerCustom } from 'Components'
import Chart from './Chart'
import Products from './Products'

export default function Home() {
	const [startDate, setStartDate] = useState(moment().subtract(7, 'days'))
	const [endDate, setEndDate] = useState(moment().subtract(1, 'days'))
	const [isMarkerInsightShow, setIsMarkerInsightShow] = useState(true)

	return (
		<Layout>
			<div className='home'>
				<div className='home__title'>
					<span>Dashboard</span>
					<DateRangePickerCustom
						startDate={startDate}
						endDate={endDate}
						onChange={(start, end) => {
							setStartDate(start)
							setEndDate(end)
						}}
					/>
				</div>

				<div className='home__tab-section'>
					<span>MARKET INSIGHTS</span>
					<div className='home__tab-section__help'>
						<Link to='/help'>
							<img src={require('Assets/icon/help.png').default} alt='' />
							Click Here for Helps
						</Link>
						<i onClick={() => setIsMarkerInsightShow(!isMarkerInsightShow)}>
							{isMarkerInsightShow ? <ExpandLess style={{ color: 'white' }} /> : <ExpandMore style={{ color: 'white' }} />}
						</i>
					</div>
				</div>

				{isMarkerInsightShow && (
					<section className='home__section'>
						<div className='home__section__summary'>
							<div>
								<span>Sales Turnover</span>
								<MoreVert />
							</div>
							<div className='home__section__summary__content'>
								<div>
									<div>Rp {new Intl.NumberFormat('id').format(360000)}</div>
									<div>
										<span>13.8% </span>
										last period in products sold
									</div>
								</div>
								<img src={require('Assets/icon/sales-turnover.svg').default} alt='' />
							</div>
						</div>
						<div className='home__section__content'>
							<div>
								<div className='home__section__content__header'>
									<span className='home__section__content__header__title'>AVERAGE PURCHASE VALUE</span>
									<select
										value={parseInt(moment(startDate).format('DM') + moment(endDate).format('DM'))}
										onChange={({ target: { value } }) => {
											if (value !== 'Custom') {
												setStartDate(inputDate.filter((item) => item.value === parseInt(value))[0].startDate)
												setEndDate(inputDate.filter((item) => item.value === parseInt(value))[0].endDate)
											}
										}}>
										<option value={null}>Custom</option>
										{inputDate.map((item) => (
											<option value={item.value}>{item.label}</option>
										))}
									</select>
									<MoreVert />
								</div>
								<Chart startDate={startDate} endDate={endDate} />
							</div>
							<div>
								<div className='home__section__content__header'>
									<span className='home__section__content__header__title'>BEST SELLING SKU</span>
									<MoreVert />
								</div>
								<Products data={bestSelling} />
							</div>
							<div>
								<div className='home__section__content__header'>
									<span className='home__section__content__header__title'>TOP COMPETITOR SKU</span>
									<MoreVert />
								</div>
								<Products data={competitor} />
							</div>
						</div>
					</section>
				)}
			</div>
		</Layout>
	)
}

//date date
let inputDate = [
	{
		label: 'Yesterday',
		value: parseInt(moment().subtract(1, 'days').format('DM') + moment().subtract(1, 'days').format('DM')),
		startDate: moment().subtract(1, 'days'),
		endDate: moment().subtract(1, 'days'),
	},
	{
		label: 'Last 7 Days',
		value: parseInt(moment().subtract(6, 'days').format('DM') + moment().subtract(1, 'days').format('DM')),
		startDate: moment().subtract(6, 'days'),
		endDate: moment().subtract(1, 'days'),
	},
	{
		label: 'Last 30 Days',
		value: parseInt(moment().subtract(29, 'days').format('DM') + moment().subtract(1, 'days').format('DM')),
		startDate: moment().subtract(29, 'days'),
		endDate: moment().subtract(1, 'days'),
	},
	{
		label: 'This Month',
		value: parseInt(moment().startOf('month').format('DM') + moment().subtract(1, 'days').format('DM')),
		startDate: moment().startOf('month'),
		endDate: moment().subtract(1, 'days'),
	},
]

//dummy data
let bestSelling = [
	{
		thumbnail: require('Assets/product/aqua.jpg').default,
		sku: 'Aqua 500 ml',
		price: '3500',
		sold: 4000,
	},
	{
		thumbnail: require('Assets/product/mizone.jpg').default,
		sku: 'Mizone 500 ml',
		price: '5000',
		sold: 3000,
	},
	{
		thumbnail: require('Assets/product/biskuat.jpg').default,
		sku: 'Biskuat',
		price: '5000',
		sold: 2000,
	},
	{
		thumbnail: require('Assets/product/levite.jpg').default,
		sku: 'Levite',
		price: '5500',
		sold: 1000,
	},
	{
		thumbnail: require('Assets/product/aqua.jpg').default,
		sku: 'Aqua 500 ml',
		price: '3500',
		sold: 4000,
	},
	{
		thumbnail: require('Assets/product/mizone.jpg').default,
		sku: 'Mizone 500 ml',
		price: '5000',
		sold: 3000,
	},
	{
		thumbnail: require('Assets/product/biskuat.jpg').default,
		sku: 'Biskuat',
		price: '5000',
		sold: 2000,
	},
	{
		thumbnail: require('Assets/product/levite.jpg').default,
		sku: 'Levite',
		price: '5500',
		sold: 1000,
	},
]

let competitor = [
	{
		thumbnail: require('Assets/product/leminerale.jpg').default,
		sku: 'Le Minerale',
		price: 3500,
		sold: 4000,
	},
	{
		thumbnail: require('Assets/product/sari-roti.jpg').default,
		sku: 'Sari Roti',
		price: 5000,
		sold: 3000,
	},
	{
		thumbnail: require('Assets/product/coca-cola.jpg').default,
		sku: 'Coca cola',
		price: 5000,
		sold: 2000,
	},
	{
		thumbnail: require('Assets/product/oreo.jpg').default,
		sku: 'Oreo',
		price: 5500,
		sold: 1000,
	},
]
