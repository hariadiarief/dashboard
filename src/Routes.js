import Home from './Pages/Home'
import Sales from './Pages/Sales'

export const publicRoutes = [
	{
		component: Home,
		path: '/',
		exact: true,
	},
	{
		component: Sales,
		path: '/sales',
		exact: true,
	},
]
