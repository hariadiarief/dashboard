import React from 'react'
import ReactDOM from 'react-dom'

import 'react-dates/lib/css/_datepicker.css'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-daterangepicker/daterangepicker.css'

import App from './App'
import './Styles/Main.scss'

ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById('root')
)
